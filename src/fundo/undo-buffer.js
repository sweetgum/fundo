export default class UndoBuffer {
  constructor({executionContext} = {}) {
    this.undoCommands = []
    this.redoCommands = []
    this.executionContext = executionContext
  }

  get canUndo() {
    return this.undoCommands.length > 0
  }

  get canRedo() {
    return this.redoCommands.length > 0
  }

  undo() {
    if (!this.undoCommands.length) {
      return
    }
    const command = this.undoCommands.shift()
    command.cancelDebouncedCommit()
    command.executeReverse(this.executionContext)
    if (command.committed) {
      command.commitReverse(this.executionContext)
    }
    this.redoCommands.unshift(command)
  }

  redo() {
    if (!this.redoCommands.length) {
      return
    }
    const command = this.redoCommands.shift()
    command.executeForward(this.executionContext)
    command.commitForward(this.executionContext)
    this.undoCommands.unshift(command)
  }

  addOrUpdate(newCommand, {autoCommit = true} = {}) {
    const current = this._currentCommand
    if (current && !current.committed && current.canUpdateFrom(newCommand)) {
      this._updateCurrentCommand(newCommand, autoCommit)
      return
    }

    this._push(newCommand, autoCommit)
  }

  get _currentCommand() {
    return this.undoCommands[0]
  }

  _push(undoCommand, autoCommit) {
    if (this._currentCommand && !this._currentCommand.committed) {
      this._currentCommand.commitForward(this.executionContext)
    }

    this.undoCommands.unshift(undoCommand)
    undoCommand.executeForward(this.executionContext)
    if (autoCommit && !undoCommand.committed) {
      undoCommand.debouncedCommitForward(this.executionContext)
    }
    this.redoCommands.length = 0
  }

  _updateCurrentCommand(undoCommand, autoCommit) {
    const current = this._currentCommand
    current.update(undoCommand)
    current.executeForward(this.executionContext)
    if (autoCommit) {
      current.debouncedCommitForward(this.executionContext)
    }
  }
}

import lolex from 'lolex'
import { Command, UndoCommand } from '../src'

describe('Undo Command', () => {
  let clock
  const debounceTime = 1000
  const undoContext = {}
  let undoCommand
  let forward
  let reverse

  beforeEach(() => {
    clock = lolex.install()

    forward = new Command()
    forward.execute = jest.fn()
    forward.executeRemote = jest.fn()
    forward.canUpdateFrom = jest.fn()
    forward.update = jest.fn()

    reverse = new Command()
    reverse.execute = jest.fn()
    reverse.executeRemote = jest.fn()

    undoCommand = new UndoCommand(forward, reverse, {
      commitDebounce: debounceTime,
      executionContext: undoContext,
    })
  })

  afterEach(() => {
    clock.uninstall()
  })

  describe('canUpdateFrom', () => {
    test('returns result of canUpdateFrom of both forward commands', () => {
      const expected = Math.random >= 0.5
      forward.canUpdateFrom.mockReturnValue(expected)

      const otherCommand = new UndoCommand(new Command(), new Command())
      const actual = undoCommand.canUpdateFrom(otherCommand)

      expect(forward.canUpdateFrom).toHaveBeenCalledWith(otherCommand.forward)
      expect(actual).toEqual(expected)
    })
  })

  describe('update', () => {
    test('updates forward command with new forward command', () => {
      const otherCommand = new UndoCommand(new Command(), new Command())
      undoCommand.update(otherCommand)

      expect(forward.update).toHaveBeenCalledWith(otherCommand.forward)
    })
  })

  describe('debouncedCommitForward', () => {
    test('starts a debounced version of commitForward', () => {
      undoCommand.debouncedCommitForward()
      expect(forward.executeRemote).not.toHaveBeenCalled()

      clock.tick(debounceTime - 1)
      undoCommand.debouncedCommitForward()
      expect(forward.executeRemote).not.toHaveBeenCalled()

      clock.tick(debounceTime)
      expect(forward.executeRemote).toHaveBeenCalled()
    })

    test('executes remote forward command with given context', () => {
      const newContext = []
      undoCommand.debouncedCommitForward(newContext)
      clock.tick(debounceTime)
      expect(forward.executeRemote).toHaveBeenCalledWith(newContext)
    })

    test('executes remote forward command with undo context if none given', () => {
      undoCommand.debouncedCommitForward()
      clock.tick(debounceTime)
      expect(forward.executeRemote).toHaveBeenCalledWith(undoContext)
    })
  })

  describe('cancelDebouncedCommit', () => {
    test('prevents the debounced commit from executing', () => {
      undoCommand.debouncedCommitForward()
      clock.tick(debounceTime - 1)
      undoCommand.cancelDebouncedCommit()
      clock.tick(2)
      expect(forward.executeRemote).not.toHaveBeenCalled()
    })
  })

  describe('commitReverse', () => {
    test('invokes reverse commands executeRemote', () => {
      undoCommand.commitReverse()
      expect(reverse.executeRemote).toHaveBeenCalled()
    })

    test('passes given context to reverse remote command', () => {
      const newContext = []
      undoCommand.commitReverse(newContext)
      expect(reverse.executeRemote).toHaveBeenCalledWith(newContext)
    })

    test('passes undo context to reverse remote command if none is given', () => {
      undoCommand.commitReverse()
      expect(reverse.executeRemote).toHaveBeenCalledWith(undoContext)
    })
  })

  describe('commitForward', () => {
    test('invokes forward commands executeRemote', () => {
      undoCommand.commitForward()
      expect(forward.executeRemote).toHaveBeenCalled()
    })

    test('marks command as committed', () => {
      undoCommand.commitForward()
      expect(undoCommand.committed).toBeTruthy()
    })

    test('cancels debouncedCommit', () => {
      undoCommand.debouncedCommitForward.cancel = jest.fn()
      undoCommand.commitForward()
      expect(undoCommand.debouncedCommitForward.cancel).toHaveBeenCalled()
    })

    test('passes given context to forward remote command', () => {
      const newContext = []
      undoCommand.commitForward(newContext)
      expect(forward.executeRemote).toHaveBeenCalledWith(newContext)
    })

    test('passes undo context to forward remote command if none is given', () => {
      undoCommand.commitForward()
      expect(forward.executeRemote).toHaveBeenCalledWith(undoContext)
    })
  })

  describe('executeForward', () => {
    test('executes forward command', () => {
      undoCommand.executeForward()
      expect(forward.execute).toHaveBeenCalled()
    })

    test('passes given context to forward command', () => {
      const newContext = []
      undoCommand.executeForward(newContext)
      expect(forward.execute).toHaveBeenCalledWith(newContext)
    })

    test('passes undo context to forward command if none is given', () => {
      undoCommand.executeForward()
      expect(forward.execute).toHaveBeenCalledWith(undoContext)
    })
  })

  describe('executeReverse', () => {
    test('executes reverse command', () => {
      undoCommand.executeReverse()
      expect(reverse.execute).toHaveBeenCalled()
    })

    test('passes given context to reverse command', () => {
      const newContext = []
      undoCommand.executeReverse(newContext)
      expect(reverse.execute).toHaveBeenCalledWith(newContext)
    })

    test('passes undo context to reverse command if none is given', () => {
      undoCommand.executeReverse()
      expect(reverse.execute).toHaveBeenCalledWith(undoContext)
    })
  })
})

import debounce from 'lodash.debounce'

export default class UndoCommand {
  constructor(forwardCommand, reverseCommand, {
    committed = false,
    commitDebounce = 0,
    executionContext,
  } = {}) {
    this.forward = forwardCommand
    this.reverse = reverseCommand
    this.committed = committed
    this.executionContext = executionContext

    this.debouncedCommitForward = debounce((context) => {
      this.commitForward(context)
    }, commitDebounce)
  }

  canUpdateFrom(otherUndoCommand) {
    return this.forward.canUpdateFrom(otherUndoCommand.forward)
  }

  update(command) {
    this.forward.update(command.forward)
  }

  cancelDebouncedCommit() {
    this.debouncedCommitForward.cancel()
  }

  executeForward(context) {
    this.forward.execute(context || this.executionContext)
  }

  executeReverse(context) {
    this.reverse.execute(context || this.executionContext)
  }

  commitForward(context) {
    this.cancelDebouncedCommit()
    this.committed = true
    this.forward.executeRemote(context || this.executionContext)
  }

  commitReverse(context) {
    this.reverse.executeRemote(context || this.executionContext)
  }
}

/* eslint-disable no-unused-vars */

export default class Command {
  canUpdateFrom(other) {
    return false
  }

  update(otherCommand) {
  }

  execute(context) {
  }

  executeRemote(context) {
  }
}

import {terser} from 'rollup-plugin-terser'
import babel from 'rollup-plugin-babel'
import pkg from './package.json'

export default {
  input: 'src/index.js',
  external: ['lodash.debounce'],
  output: {
    file: pkg.main,
    format: 'cjs'
  },
  plugins: [
    babel(),
    terser(),
  ]
}

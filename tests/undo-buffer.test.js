import { UndoBuffer, UndoCommand, Command } from '../src'

describe('Undo Buffer', () => {
  let buffer
  let command
  const context = {}

  beforeEach(() => {
    buffer = new UndoBuffer({executionContext: context})
    command = new UndoCommand(new Command(), new Command())
  })

  describe('canUndo', () => {
    test('is false when buffer is empty', () => {
      expect(buffer.canUndo).toBeFalsy()
    })

    test('is true when commands are available to undo', () => {
      buffer.addOrUpdate(command)
      expect(buffer.canUndo).toBeTruthy()
    })

    test('is false when commands are not available to undo', () => {
      buffer.addOrUpdate(command)
      buffer.undo()
      expect(buffer.canUndo).toBeFalsy()
    })
  })

  describe('canRedo', () => {
    test('is false when buffer is empty', () => {
      expect(buffer.canRedo).toBeFalsy()
    })

    test('is true when commands are available to redo', () => {
      buffer.addOrUpdate(command)
      buffer.undo()
      expect(buffer.canRedo).toBeTruthy()
    })

    test('is false when commands are not available to redo', () => {
      buffer.addOrUpdate(command)
      buffer.undo()
      buffer.redo()
      expect(buffer.canRedo).toBeFalsy()
    })
  })

  describe('addOrUpdate', () => {
    test('updates previous command if not committed and compatible with new command', () => {
      const oldCommand = new UndoCommand(new Command(), new Command())
      oldCommand.forward.update = jest.fn()
      oldCommand.forward.canUpdateFrom = jest.fn()
      oldCommand.forward.canUpdateFrom.mockReturnValue(true)
      buffer.addOrUpdate(oldCommand)
      buffer.addOrUpdate(command)
      expect(oldCommand.forward.update).toHaveBeenCalled()
    })

    test('does not update previous command if it was already committed', () => {
      const oldCommand = new UndoCommand(new Command(), new Command())
      oldCommand.forward.update = jest.fn()
      oldCommand.commitForward()
      oldCommand.forward.canUpdateFrom = jest.fn()
      oldCommand.forward.canUpdateFrom.mockReturnValue(true)
      buffer.addOrUpdate(oldCommand)
      buffer.addOrUpdate(command)
      expect(oldCommand.forward.update).not.toHaveBeenCalled()
    })

    test('does not update previous command if it is not compatible with new command', () => {
      const oldCommand = new UndoCommand(new Command(), new Command())
      oldCommand.forward.update = jest.fn()
      oldCommand.forward.canUpdateFrom = jest.fn()
      oldCommand.forward.canUpdateFrom.mockReturnValue(false)
      buffer.addOrUpdate(oldCommand)
      buffer.addOrUpdate(command)
      expect(oldCommand.forward.update).not.toHaveBeenCalled()
    })

    describe('adding new command', () => {
      test('executes the command forward with context', () => {
        command.forward.execute = jest.fn()
        buffer.addOrUpdate(command)
        expect(command.forward.execute).toHaveBeenCalledWith(context)
      })

      test('starts a debounced forward commit with context', () => {
        command.debouncedCommitForward = jest.fn()
        buffer.addOrUpdate(command)
        expect(command.debouncedCommitForward).toHaveBeenCalledWith(context)
      })

      test('prevents redoing old commands', () => {
        const oldCommand = new UndoCommand(new Command(), new Command())
        buffer.addOrUpdate(oldCommand)
        buffer.undo()

        buffer.addOrUpdate(command)
        expect(buffer.canRedo).toBeFalsy()
      })

      describe('and previous command is not comitted', () => {
        test('commits previous command forward with context', () => {
          const oldCommand = new UndoCommand(new Command(), new Command())
          oldCommand.commitForward = jest.fn()
          buffer.addOrUpdate(oldCommand)
          buffer.addOrUpdate(command)
          expect(oldCommand.commitForward).toHaveBeenCalledWith(context)
        })
      })

      describe('with pre-committed command', () => {
        test('executes the command forward with context', () => {
          command = new UndoCommand(new Command(), new Command(), {committed: true})
          command.forward.execute = jest.fn()
          buffer.addOrUpdate(command)
          expect(command.forward.execute).toHaveBeenCalledWith(context)
        })

        test('does not start debounced commit', () => {
          command = new UndoCommand(new Command(), new Command(), {committed: true})
          command.debouncedCommitForward = jest.fn()
          buffer.addOrUpdate(command)
          expect(command.debouncedCommitForward).not.toHaveBeenCalled()
        })
      })

      describe('with auto commit option false', () => {
        test('executes the command forward with context', () => {
          command.forward.execute = jest.fn()
          buffer.addOrUpdate(command, {autoCommit: false})
          expect(command.forward.execute).toHaveBeenCalledWith(context)
        })

        test('does not start debounced commit', () => {
          command.debouncedCommitForward = jest.fn()
          buffer.addOrUpdate(command, {autoCommit: false})
          expect(command.debouncedCommitForward).not.toHaveBeenCalled()
        })

        test('does not commit forward', () => {
          command.commitForward = jest.fn()
          buffer.addOrUpdate(command, {autoCommit: false})
          expect(command.commitForward).not.toHaveBeenCalled()
        })
      })
    })

    describe('updating previous command', () => {
      let oldCommand

      beforeEach(() => {
        oldCommand = new UndoCommand(new Command(), new Command())
        oldCommand.forward.canUpdateFrom = jest.fn()
        oldCommand.forward.canUpdateFrom.mockReturnValue(true)
        buffer.addOrUpdate(oldCommand)
      })

      test('updates previous command with new one', () => {
        oldCommand.update = jest.fn()
        buffer.addOrUpdate(command)
        expect(oldCommand.update).toHaveBeenCalled()
      })

      test('executes the forward command with context', () => {
        oldCommand.forward.execute = jest.fn()
        buffer.addOrUpdate(command)
        expect(oldCommand.forward.execute).toHaveBeenCalledWith(context)
      })

      test('executes debounced forward commit with context', () => {
        oldCommand.debouncedCommitForward = jest.fn()
        buffer.addOrUpdate(command)
        expect(oldCommand.debouncedCommitForward).toHaveBeenCalledWith(context)
      })

      describe('with auto commit option false', () => {
        test('executes the forward command with context', () => {
          oldCommand.forward.execute = jest.fn()
          buffer.addOrUpdate(command, {autoCommit: false})
          expect(oldCommand.forward.execute).toHaveBeenCalledWith(context)
        })

        test('does not start debounced forward commit', () => {
          oldCommand.debouncedCommitForward = jest.fn()
          buffer.addOrUpdate(command, {autoCommit: false})
          expect(oldCommand.debouncedCommitForward).not.toHaveBeenCalled()
        })

        test('does not commit forward', () => {
          oldCommand.commitForward = jest.fn()
          buffer.addOrUpdate(command, {autoCommit: false})
          expect(oldCommand.commitForward).not.toHaveBeenCalled()
        })
      })
    })
  })

  describe('undo when empty', () => {
    test('has no effect', () => {
      buffer.undo()
    })
  })

  describe('undo', () => {
    beforeEach(() => {
      buffer.addOrUpdate(command)
    })

    test('cancels current command debounced commit', () => {
      command.cancelDebouncedCommit = jest.fn()
      buffer.undo()
      expect(command.cancelDebouncedCommit).toHaveBeenCalled()
    })

    test('executes current command reverse with context', () => {
      command.reverse.execute = jest.fn()
      buffer.undo()
      expect(command.reverse.execute).toHaveBeenCalledWith(context)
    })

    test('commits command reverse with context if it was committed', () => {
      command.commitReverse = jest.fn()
      command.commitForward()
      buffer.undo()
      expect(command.commitReverse).toHaveBeenCalledWith(context)
    })

    test('does not commit command reverse if it was not committed', () => {
      command.commitReverse = jest.fn()
      buffer.undo()
      expect(command.commitReverse).not.toHaveBeenCalled()
    })
  })

  describe('redo when empty', () => {
    test('has no effect', () => {
      buffer.redo()
    })
  })

  describe('redo', () => {
    beforeEach(() => {
      buffer.addOrUpdate(command)
      buffer.undo()
    })

    test('executes the next redo command with context', () => {
      command.forward.execute = jest.fn()
      buffer.redo()
      expect(command.forward.execute).toHaveBeenCalledWith(context)
    })

    test('commits the next redo command forward with context', () => {
      command.commitForward = jest.fn()
      buffer.redo()
      expect(command.commitForward).toHaveBeenCalledWith(context)
    })
  })
})
